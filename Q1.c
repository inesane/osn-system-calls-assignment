#include <stdio.h>
#include <fcntl.h>
#include <string.h>
#include <sys/stat.h>
#include <unistd.h>
#include <errno.h>

extern int errno;

int main(int argc, char *argv[])
{
    long long int i = 1, j, length, fd, sz, mem = 1000000, fd2, sz2, loc, finished = 0, total, temp, start, end, k, count = 0;
    if (argc != 2)
    {
        char error[] = "ERROR: Wrong number of inputs\n";
        write(1, error, strlen(error));
        return 1;
    }
    fd = open(argv[1], O_RDONLY);
    if (fd == -1)
    {
        perror("ERROR");
        return 1;
    }
    length = strlen(argv[1]);
    for (j = length - 1; j >= 0; j--)
    {
        if (argv[1][j] == '/')
        {
            break;
        }
    }
    char filearr[length+20];
    for (k = j + 1; k < length; k++)
    {
        filearr[count] = argv[1][k];
        count++;
    }
    filearr[count] = '\0';
    char file[2000] = "Assignment/";
    strcat(file, filearr);
    mkdir("Assignment", 0700);
    total = lseek(fd, 0, SEEK_END);
    fd2 = open(file, O_TRUNC | O_WRONLY | O_CREAT, 0600);
    if (fd2 == -1)
    {
        perror("ERROR");
        return 1;
    }
    while (i > 0)
    {
        if (total - i * mem < 0)
        {
            lseek(fd, 0, SEEK_SET);
            finished = 1;
            mem = total - (i - 1) * mem;
            loc = -mem;
        }
        else if (i == 1)
        {
            loc = -1 * mem;
            lseek(fd, loc, SEEK_CUR);
        }
        else
        {
            loc = -2 * mem;
            lseek(fd, loc, SEEK_CUR);
        }
        char BUFFER[mem + 5];
        sz = read(fd, BUFFER, mem);
        BUFFER[sz] = '\0';
        start = 0;
        end = sz - 1;
        while (start < end)
        {
            temp = BUFFER[start];
            BUFFER[start] = BUFFER[end];
            BUFFER[end] = temp;
            start++;
            end--;
        }
        sz2 = write(fd2, BUFFER, mem);
        i++;
        if (finished == 1)
            break;
        char perc[7];
        float ans = (float)((float)i * (float)mem / (float)total);
        ans = ans * 10000.00;
        int intans = ans;
        perc[6] = '\r';
        perc[5] = '%';
        perc[2] = '.';
        int hundredths = intans % 10;
        int tenths = (intans % 100 - hundredths) / 10;
        int ones = (intans % 1000 - 10 * tenths - hundredths) / 100;
        int tens = (intans % 10000 - 100 * ones - 10 * tenths - hundredths) / 1000;
        perc[4] = hundredths + '0';
        perc[3] = tenths + '0';
        perc[1] = ones + '0';
        perc[0] = tens + '0';
        write(1, perc, 7);
    }
    char done[8] = "100.00%\n";
    write(1, done, 8);
    close(fd);
    close(fd2);
}
