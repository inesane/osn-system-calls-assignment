#include <stdio.h>
#include <fcntl.h>
#include <string.h>
#include <sys/stat.h>
#include <unistd.h>
#include <errno.h>

extern int errno;

int main(int argc, char *argv[])
{
    long long int dir1 = 0, dir2 = 0, dir3 = 0, fd1, fd2, total1, total2, reversed = 1, i = 1, mem = 1000005, loc, sz1, sz2, j, loop = 1, file1 = 1, file2 = 1;
    if (argc != 4)
    {
        char error[] = "ERROR: Wrong number of inputs\n";
        write(1, error, strlen(error));
        return 1;
    }

    struct stat stats1;
    stat(argv[1], &stats1);
    if (S_ISDIR(stats1.st_mode))
    {
        dir1 = 1;
        char error1[] = "ERROR: First input is not a file\n";
        write(1, error1, strlen(error1));
    }

    struct stat stats2;
    stat(argv[2], &stats2);
    if (S_ISDIR(stats2.st_mode))
    {
        dir2 = 1;
        char error2[] = "ERROR: Second input is not a file\n";
        write(1, error2, strlen(error2));
    }

    struct stat stats;
    stat(argv[3], &stats);
    if (S_ISDIR(stats.st_mode))
    {
        dir3 = 1;
    }

    fd1 = open(argv[1], O_RDONLY, 0600);
    if (fd1 == -1)
    {
        perror("ERROR");
        file1 = 0;
    }

    fd2 = open(argv[2], O_RDONLY, 0600);
    if (fd2 == -1)
    {
        perror("ERROR");
        file2 = 0;
    }

    total1 = lseek(fd1, 0, SEEK_END);
    total2 = lseek(fd2, 0, SEEK_END);
    lseek(fd1, 0, SEEK_SET);
    if (file1 == 1 && file2 == 1 && dir1 == 0 && dir2 == 0)
    {
        if (total1 != total2)
        {
            reversed = 0;
        }
        else
        {
            while (i > 0)
            {
                if (total1 - i * mem <= 0)
                {
                    lseek(fd2, 0, SEEK_SET);
                    mem = total1 - (i - 1) * mem;
                    loc = -mem;
                    loop = 0;
                }
                else if (i == 1)
                {
                    loc = -1 * mem;
                    lseek(fd2, loc, SEEK_CUR);
                }
                else
                {
                    loc = -2 * mem;
                    lseek(fd2, loc, SEEK_CUR);
                }
                char BUFFER1[mem + 5], BUFFER2[mem + 5];
                sz1 = read(fd1, BUFFER1, mem);
                sz2 = read(fd2, BUFFER2, mem);
                for (j = 0; j < sz2; j++)
                {
                    if (BUFFER1[j] != BUFFER2[sz2 - j - 1])
                    {
                        reversed = 0;
                    }
                    if (reversed == 0)
                    {
                        break;
                    }
                }
                if (reversed == 0 || loop == 0)
                    break;
                i++;
            }
        }
    }

    if (dir3 == 1)
    {
        char dire[] = "Directory is created: Yes\n";
        write(1, dire, 26);
    }
    else
    {
        char dire[] = "Directory is created: No\n";
        write(1, dire, 25);
    }

    if (file1 == 1 && file2 == 1 && dir1 == 0 && dir2 == 0)
    {
        if (reversed == 1)
        {
            char reve[] = "Whether file contents are reversed in newfile: Yes\n";
            write(1, reve, 51);
        }
        else
        {
            char reve[] = "Whether file contents are reversed in newfile: No\n";
            write(1, reve, 50);
        }
    }

    int irusr, iwusr, ixusr, irgrp, iwgrp, ixgrp, iroth, iwoth, ixoth;

    if (dir1 == 0 && file1 == 1)
    {
        struct stat stat1;
        if (stat(argv[1], &stat1) < 0)
            return 1;

        irusr = stat1.st_mode & S_IRUSR;
        iwusr = stat1.st_mode & S_IWUSR;
        ixusr = stat1.st_mode & S_IXUSR;
        irgrp = stat1.st_mode & S_IRGRP;
        iwgrp = stat1.st_mode & S_IWGRP;
        ixgrp = stat1.st_mode & S_IXGRP;
        iroth = stat1.st_mode & S_IROTH;
        iwoth = stat1.st_mode & S_IWOTH;
        ixoth = stat1.st_mode & S_IXOTH;

        if ((irusr) > 0)
        {
            char out[] = "User has read permissions on newfile: Yes\n";
            write(1, out, strlen(out));
        }
        else
        {
            char out[] = "User has read permissions on newfile: No\n";
            write(1, out, strlen(out));
        }

        if ((iwusr) > 0)
        {
            char out[] = "User has write permission on newfile: Yes\n";
            write(1, out, strlen(out));
        }
        else
        {
            char out[] = "User has write permission on newfile: No\n";
            write(1, out, strlen(out));
        }

        if ((ixusr) > 0)
        {
            char out[] = "User has execute permission on newfile: Yes\n";
            write(1, out, strlen(out));
        }
        else
        {
            char out[] = "User has execute permission on newfile: No\n";
            write(1, out, strlen(out));
        }

        if ((irgrp) > 0)
        {
            char out[] = "Group has read permissions on newfile: Yes\n";
            write(1, out, strlen(out));
        }
        else
        {
            char out[] = "Group has read permissions on newfile: No\n";
            write(1, out, strlen(out));
        }

        if ((iwgrp) > 0)
        {
            char out[] = "Group has write permission on newfile: Yes\n";
            write(1, out, strlen(out));
        }
        else
        {
            char out[] = "Group has write permission on newfile: No\n";
            write(1, out, strlen(out));
        }

        if ((ixgrp) > 0)
        {
            char out[] = "Group has execute permission on newfile: Yes\n";
            write(1, out, strlen(out));
        }
        else
        {
            char out[] = "Group has execute permission on newfile: No\n";
            write(1, out, strlen(out));
        }

        if ((iroth) > 0)
        {
            char out[] = "Others has read permissions on newfile: Yes\n";
            write(1, out, strlen(out));
        }
        else
        {
            char out[] = "Others has read permissions on newfile: No\n";
            write(1, out, strlen(out));
        }

        if ((iwoth) > 0)
        {
            char out[] = "Others has write permission on newfile: Yes\n";
            write(1, out, strlen(out));
        }
        else
        {
            char out[] = "Others has write permission on newfile: No\n";
            write(1, out, strlen(out));
        }

        if ((ixoth) > 0)
        {
            char out[] = "Others has execute permission on newfile: Yes\n";
            write(1, out, strlen(out));
        }
        else
        {
            char out[] = "Others has execute permission on newfile: No\n";
            write(1, out, strlen(out));
        }
    }

    if (dir2 == 0 && file2 == 1)
    {
        struct stat stat2;
        if (stat(argv[2], &stat2) < 0)
            return 1;

        irusr = stat2.st_mode & S_IRUSR;
        iwusr = stat2.st_mode & S_IWUSR;
        ixusr = stat2.st_mode & S_IXUSR;
        irgrp = stat2.st_mode & S_IRGRP;
        iwgrp = stat2.st_mode & S_IWGRP;
        ixgrp = stat2.st_mode & S_IXGRP;
        iroth = stat2.st_mode & S_IROTH;
        iwoth = stat2.st_mode & S_IWOTH;
        ixoth = stat2.st_mode & S_IXOTH;

        if ((irusr) > 0)
        {
            char out[] = "User has read permissions on oldfile: Yes\n";
            write(1, out, strlen(out));
        }
        else
        {
            char out[] = "User has read permissions on oldfile: No\n";
            write(1, out, strlen(out));
        }

        if ((iwusr) > 0)
        {
            char out[] = "User has write permission on oldfile: Yes\n";
            write(1, out, strlen(out));
        }
        else
        {
            char out[] = "User has write permission on oldfile: No\n";
            write(1, out, strlen(out));
        }

        if ((ixusr) > 0)
        {
            char out[] = "User has execute permission on oldfile: Yes\n";
            write(1, out, strlen(out));
        }
        else
        {
            char out[] = "User has execute permission on oldfile: No\n";
            write(1, out, strlen(out));
        }

        if ((irgrp) > 0)
        {
            char out[] = "Group has read permissions on oldfile: Yes\n";
            write(1, out, strlen(out));
        }
        else
        {
            char out[] = "Group has read permissions on oldfile: No\n";
            write(1, out, strlen(out));
        }

        if ((iwgrp) > 0)
        {
            char out[] = "Group has write permission on oldfile: Yes\n";
            write(1, out, strlen(out));
        }
        else
        {
            char out[] = "Group has write permission on oldfile: No\n";
            write(1, out, strlen(out));
        }

        if ((ixgrp) > 0)
        {
            char out[] = "Group has execute permission on oldfile: Yes\n";
            write(1, out, strlen(out));
        }
        else
        {
            char out[] = "Group has execute permission on oldfile: No\n";
            write(1, out, strlen(out));
        }

        if ((iroth) > 0)
        {
            char out[] = "Others has read permissions on oldfile: Yes\n";
            write(1, out, strlen(out));
        }
        else
        {
            char out[] = "Others has read permissions on oldfile: No\n";
            write(1, out, strlen(out));
        }

        if ((iwoth) > 0)
        {
            char out[] = "Others has write permission on oldfile: Yes\n";
            write(1, out, strlen(out));
        }
        else
        {
            char out[] = "Others has write permission on oldfile: No\n";
            write(1, out, strlen(out));
        }

        if ((ixoth) > 0)
        {
            char out[] = "Others has execute permission on oldfile: Yes\n";
            write(1, out, strlen(out));
        }
        else
        {
            char out[] = "Others has execute permission on oldfile: No\n";
            write(1, out, strlen(out));
        }
    }

    if (dir3 == 1)
    {
        struct stat stat3;
        if (stat(argv[3], &stat3) < 0)
            return 1;
        irusr = stat3.st_mode & S_IRUSR;
        iwusr = stat3.st_mode & S_IWUSR;
        ixusr = stat3.st_mode & S_IXUSR;
        irgrp = stat3.st_mode & S_IRGRP;
        iwgrp = stat3.st_mode & S_IWGRP;
        ixgrp = stat3.st_mode & S_IXGRP;
        iroth = stat3.st_mode & S_IROTH;
        iwoth = stat3.st_mode & S_IWOTH;
        ixoth = stat3.st_mode & S_IXOTH;

        if ((irusr) > 0)
        {
            char out[] = "User has read permissions on directory: Yes\n";
            write(1, out, strlen(out));
        }
        else
        {
            char out[] = "User has read permissions on directory: No\n";
            write(1, out, strlen(out));
        }

        if ((iwusr) > 0)
        {
            char out[] = "User has write permission on directory: Yes\n";
            write(1, out, strlen(out));
        }
        else
        {
            char out[] = "User has write permission on directory: No\n";
            write(1, out, strlen(out));
        }

        if ((ixusr) > 0)
        {
            char out[] = "User has execute permission on directory: Yes\n";
            write(1, out, strlen(out));
        }
        else
        {
            char out[] = "User has execute permission on directory: No\n";
            write(1, out, strlen(out));
        }

        if ((irgrp) > 0)
        {
            char out[] = "Group has read permissions on directory: Yes\n";
            write(1, out, strlen(out));
        }
        else
        {
            char out[] = "Group has read permissions on directory: No\n";
            write(1, out, strlen(out));
        }

        if ((iwgrp) > 0)
        {
            char out[] = "Group has write permission on directory: Yes\n";
            write(1, out, strlen(out));
        }
        else
        {
            char out[] = "Group has write permission on directory: No\n";
            write(1, out, strlen(out));
        }

        if ((ixgrp) > 0)
        {
            char out[] = "Group has execute permission on directory: Yes\n";
            write(1, out, strlen(out));
        }
        else
        {
            char out[] = "Group has execute permission on directory: No\n";
            write(1, out, strlen(out));
        }

        if ((iroth) > 0)
        {
            char out[] = "Others has read permissions on directory: Yes\n";
            write(1, out, strlen(out));
        }
        else
        {
            char out[] = "Others has read permissions on directory: No\n";
            write(1, out, strlen(out));
        }

        if ((iwoth) > 0)
        {
            char out[] = "Others has write permission on directory: Yes\n";
            write(1, out, strlen(out));
        }
        else
        {
            char out[] = "Others has write permission on directory: No\n";
            write(1, out, strlen(out));
        }

        if ((ixoth) > 0)
        {
            char out[] = "Others has execute permission on directory: Yes\n";
            write(1, out, strlen(out));
        }
        else
        {
            char out[] = "Others has execute permission on directory: No\n";
            write(1, out, strlen(out));
        }
    }
}